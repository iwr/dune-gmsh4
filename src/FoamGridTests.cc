// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/grid/utility/structuredgridfactory.hh>

#include <dune/vtk/vtkreader.hh>
#include <dune/vtk/gridcreators/lagrangegridcreator.hh>
#include <dune/vtk/writers/vtkunstructuredgridwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>

#include <dune/gmsh4/gmsh4reader.hh>
#include <dune/gmsh4/gridcreators/continuousgridcreator.hh>
#include <dune/gmsh4/gridcreators/lagrangegridcreator.hh>

#include <dune/foamgrid/foamgrid.hh>
#include <dune/curvedsurfacegrid/curvedsurfacegrid.hh>

using namespace Dune;

template <int p>
constexpr std::integral_constant<int,p> order_c = {};

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  using GridType = FoamGrid<2, 3>;


  //triangle-pair

  /*{ //test 01: triangle-pair: vtk -> vtk
    std::cout << "triangle-pair: vtk -> vtk (curved, order 3)" << std::endl;
    GridFactory<GridType> factory;
    LagrangeGridCreator creator(factory);

    VtkReader reader(creator);
    reader.read("dune-vtk/doc/triangles_3d_order3_subset.vtu");
    std::unique_ptr gridPtr = factory.createGrid();
    auto& grid = *gridPtr;

    CurvedSurfaceGrid curvedGrid(grid, creator, order_c<3>);

    Vtk::LagrangeDataCollector dataCollector(curvedGrid.leafGridView(), 3);
    VtkUnstructuredGridWriter vtkWriter(dataCollector, Vtk::ASCII);
    vtkWriter.write("FG_test01_triangles_order3_subset_VtkReader.vtu");
  }*/

  { //test 02: triangle-pair: gmsh4 -> vtk (flat, order 3)
    std::cout << "\ntriangle-pair: gmsh4 -> vtk (flat, order 3)" << std::endl;
    std::unique_ptr gridPtr = Gmsh4Reader<GridType>::createGridFromFile(
               GRID_PATH "/triangles_3d_order3_subset.msh");
    auto& grid = *gridPtr;

    Vtk::LagrangeDataCollector dataCollector(grid.leafGridView(), 3);
    VtkUnstructuredGridWriter vtkWriter(dataCollector, Vtk::ASCII);
    vtkWriter.write("FG_test02_triangles_order3_subset_flat.vtu");
  }

  { //test 03: triangle-pair: gmsh4 -> vtk (curved, order 3)
    std::cout << "\ntriangle-pair: gmsh4 -> vtk (curved, order 3)" << std::endl;
    GridFactory<GridType> factory;
    Gmsh4::LagrangeGridCreator creator(factory);

    Gmsh4Reader reader(creator);
    reader.read(GRID_PATH "/triangles_3d_order3_subset.msh");
    std::unique_ptr gridPtr = factory.createGrid();
    auto& grid = *gridPtr;

    CurvedSurfaceGrid curvedGrid(grid, creator, order_c<3>);

    Vtk::LagrangeDataCollector dataCollector(curvedGrid.leafGridView(), 3);
    VtkUnstructuredGridWriter vtkWriter(dataCollector, Vtk::ASCII);
    vtkWriter.write("FG_test03_triangles_order3_subset_curved.vtu");
  }


  //sphere ascii

  { //test 04: sphere: gmsh4 -> vtk (flat, order 4)
    std::cout << "\nsphere: gmsh4 -> vtk (flat, order 4, ascii)" << std::endl;
    std::unique_ptr gridPtr = Gmsh4Reader<GridType>::createGridFromFile(
                               GRID_PATH "/sphere_order4.msh");
    auto& grid = *gridPtr;

    Vtk::LagrangeDataCollector dataCollector(grid.leafGridView(), 4);
    VtkUnstructuredGridWriter vtkWriter(dataCollector, Vtk::ASCII);
    vtkWriter.write("FG_test04_sphere_order4_ascii_flat.vtu");
  }

  { //test 05: sphere: gmsh4 -> vtk (curved, order 4)
    std::cout << "\nsphere: gmsh4 -> vtk (curved, order 4, ascii)" << std::endl;
    GridFactory<GridType> factory;
    Gmsh4::LagrangeGridCreator creator(factory);

    Gmsh4Reader reader(creator);
    reader.read(GRID_PATH "/sphere_order4.msh");
    std::unique_ptr gridPtr = factory.createGrid();
    auto& grid = *gridPtr;

    CurvedSurfaceGrid curvedGrid(grid, creator, order_c<4>);

    Vtk::LagrangeDataCollector dataCollector(curvedGrid.leafGridView(), 4);
    VtkUnstructuredGridWriter vtkWriter(dataCollector, Vtk::ASCII);
    vtkWriter.write("FG_test05_sphere_order4_ascii_curved.vtu");
  }


  // sphere binary

  { //test 06: sphere: gmsh4 -> vtk (flat, order 1, binary)
    std::cout << "\nsphere: gmsh4 -> vtk (flat, order 1, binary)" << std::endl;
    std::unique_ptr gridPtr = Gmsh4Reader<GridType>::createGridFromFile(
                        GRID_PATH "/sphere_order1_binary.msh");
    auto& grid = *gridPtr;

    VtkUnstructuredGridWriter vtkWriter(grid.leafGridView(), Vtk::ASCII);
    vtkWriter.write("FG_test06_sphere_order1_binary.vtu");
  }

  { //test 07: sphere: gmsh4 -> vtk (flat, order 4, binary)
    std::cout << "\nsphere: gmsh4 -> vtk (flat, order 4, binary)" << std::endl;
    std::unique_ptr gridPtr = Gmsh4Reader<GridType>::createGridFromFile(
                        GRID_PATH "/sphere_order4_binary.msh");
    auto& grid = *gridPtr;

    Vtk::LagrangeDataCollector dataCollector(grid.leafGridView(), 4);
    VtkUnstructuredGridWriter vtkWriter(dataCollector, Vtk::ASCII);
    vtkWriter.write("FG_test07_sphere_order4_binary_flat.vtu");
  }

  { //test 08: sphere: gmsh4 -> vtk (curved, order 4, binary)
    std::cout << "\nsphere: gmsh4 -> vtk (curved, order 4, binary)" << std::endl;
    GridFactory<GridType> factory;
    Gmsh4::LagrangeGridCreator creator(factory);

    Gmsh4Reader reader(creator);
    reader.read(GRID_PATH "/sphere_order4_binary.msh");
    std::unique_ptr gridPtr = factory.createGrid();
    auto& grid = *gridPtr;

    CurvedSurfaceGrid curvedGrid(grid, creator, order_c<4>);

    Vtk::LagrangeDataCollector dataCollector(curvedGrid.leafGridView(), 4);
    VtkUnstructuredGridWriter vtkWriter(dataCollector, Vtk::ASCII);
    vtkWriter.write("FG_test08_sphere_order4_binary_curved.vtu");
  }

  return 0;
}
