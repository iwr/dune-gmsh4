// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <iostream>
#include <vector>

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/common/filledarray.hh>

//#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

#include <dune/gmsh4/gmsh4reader.hh>
#include <dune/gmsh4/gridcreators/continuousgridcreator.hh>
#include <dune/gmsh4/gridcreators/discontinuousgridcreator.hh>
#include <dune/gmsh4/utility/version.hh>

#include <dune/alugrid/grid.hh>

using namespace Dune;

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  const int dim = 2;

  //using GridType = UGGrid<dim>;
  using GridType = Dune::ALUGrid<2,2,Dune::simplex,Dune::conforming>;
  using GridView = typename GridType::LeafGridView;
  {
    auto ver = Gmsh4::fileVersion(GRID_PATH "/square.msh");
    if (ver[0] != 4)
      DUNE_THROW(Dune::Exception, "Wrong .msh file version");

    auto gridPtr = Gmsh4Reader<GridType>::createGridFromFile(GRID_PATH "/square.msh");
    auto& grid = *gridPtr;

    VTKWriter<GridView> vtkWriter(grid.leafGridView());
    vtkWriter.write("square.vtu");
  }

  {
    auto gridPtr = Gmsh4Reader<GridType>::createGridFromFile(GRID_PATH "/square_part1_topology.pro");
    auto& grid = *gridPtr;

    VTKWriter<GridView> vtkWriter(grid.leafGridView());
    vtkWriter.write("square_part1.vtu");
  }

  {
    auto ver = Gmsh4::fileVersion(GRID_PATH "/square_part2.msh");
    if (ver[0] != 4)
      DUNE_THROW(Dune::Exception, "Wrong .msh file version");

    auto gridPtr = Gmsh4Reader<GridType>::createGridFromFile(GRID_PATH "/square_part2.msh");
    auto& grid = *gridPtr;

    VTKWriter<GridView> vtkWriter(grid.leafGridView());
    vtkWriter.write("square_part2.vtu");
  }
}
