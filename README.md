# Dune-Gmsh4
File reader for the Gmsh MSH file format version 4.

## Summary
Provides a file reader to read MSH file format version 4. It supports various
grid-creator policies. Higher-order parametrized grids can be read using a
`LagrangeGridCreator`.

## Mirror
This repositories is mirrored to https://gitlab.com/iwr/dune-gmsh4.

## Installation Instructions
`dune-gmsh4` requires the DUNE core modules, version 2.9 or later.
Please see the [general instructions for building DUNE modules](https://www.dune-project.org/doc/installation)
for detailed instructions on how to build the module.

## Usage
Either construct a grid directly
```c++
#include <dune/gmsh4/gmsh4reader.hh>
#include <dune/grid/uggrid.hh>

int main()
{
  using namespace Dune;
  using Grid = UGGrid<2>;
  std::unique_ptr grid = Gmsh4Reader<Grid>::createGridFromFile("filename.msh");
}
```

or explicitly use a grid-creator
```c++
#include <dune/foamgrid/foamgrid.hh>
#include <dune/gmsh4/gmsh4reader.hh>
#include <dune/gmsh4/gridcreators/lagrangegridcreator.hh>

int main()
{
  using namespace Dune;
  using Grid = FoamGrid<2,3>;
  GridFactory<Grid> factory;
  Gmsh4::LagrangeGridCreator creator{factory};
  Gmsh4Reader reader{creator};

  reader.read("filename.msh");
  auto grid = factory.createGrid();
}
```
Note, the grid-creator itself is a grid-function.
