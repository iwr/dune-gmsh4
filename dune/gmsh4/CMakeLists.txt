target_sources(dunegmsh4 PRIVATE
  types.cc)

#install headers
install(FILES
  filereader.hh
  gmsh4reader.hh
  gmsh4reader.impl.hh
  gridcreatorinterface.hh
  types.hh
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/gmsh4)

add_subdirectory(gridcreators)
add_subdirectory(utility)
